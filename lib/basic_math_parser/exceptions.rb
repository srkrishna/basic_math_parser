module BasicMathParser
  class Error < StandardError; end

  class ParseError < Error
    attr_reader :error, :info

   def initialize(error, **info)
     @error = error
     @info = info
   end

    private_class_method :new

    def self.for(reason, **info)
      new(reason, **info)
    end
  end
end
