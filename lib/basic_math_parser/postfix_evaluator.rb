require_relative './exceptions'

class PostfixEvaluator
  def run(tokens)
    @numbers = []

    tokens.each do |token|
      push_number(token)        if token.type == :number
      evaluate_operation(token) if token.type == :op
    end

    @numbers.last.value
  end

  private

  def push_number(token)
    @numbers << token
  end

  def evaluate_operation(token)
    right_num = @numbers.pop
    left_num  = @numbers.pop

    raise BasicMathParser::ParseError.for('Invalid postfix expression', info: { right: right_num, left: left_num } ) unless right_num && left_num

    result = evaluate(left_num.value, right_num.value, token)
    @numbers << Token.new(:number, result)
  end

  def evaluate(left_num, right_num, operation)
    case operation.value
    when '+' then left_num + right_num
    when '-' then left_num - right_num
    when '*' then left_num * right_num
    when '/' then left_num / right_num
    when '%' then left_num % right_num
    when '^' then left_num ** right_num

    end
  end
end
