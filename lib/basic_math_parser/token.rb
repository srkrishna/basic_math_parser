class Token
  attr_reader :type

  def initialize(type, value)
    @type, @value = type, value
  end

  def ==(other)
   value == other.value &&
   type  == other.type
  end

  def value
    @type == :number ? @value.to_f : @value
  end

  def priority
    return 0 if value == '(' || value == ')'
    return 1 if value == '+' || value == '-'
    return 2 if value == '*' || value == '/'
    return 3 if value == '%'
    return 4 if value == '^'
  end
end
