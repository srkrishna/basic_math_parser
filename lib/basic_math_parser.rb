# frozen_string_literal: true
require 'strscan'
require 'timeout'

require_relative "basic_math_parser/version"
require_relative "basic_math_parser/postfix_evaluator"
require_relative "basic_math_parser/parser"
require_relative "basic_math_parser/tokenizer"

module BasicMathParser
  def self.evaluate(expression)
    status = Timeout::timeout(5) {
      # Parse input string
      lexer  = Tokenizer.new
      tokens = lexer.parse(expression)

      # Convert to postfix notation
      postfix_exp = Parser.new(tokens).run

      # Evaluate
      PostfixEvaluator.new.run(postfix_exp.output)
    }
  end
end
