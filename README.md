# BasicMathParser

BasicMathParser is an simple expression parser built based on the postfix expression parsing.

## Installation

Install the gem and add to the application's Gemfile by executing:

    $ bundle add basic_math_parser

If bundler is not being used to manage dependencies, install the gem by executing:

    $ gem install basic_math_parser

## Examples

To parse an expression:
```
BasicMathParser.evaluate("3+5")
# => 8.0
```
BasicMathParser will support Order precedence and use of parentheses to group expressions, by this Proper evaluation is ensured.
```
BasicMathParser.evaluate("(10 + 5) / 5")
# => 3.0

BasicMathParser.evaluate("10 + 5 / 5")
# => 11.0
```

**Supported operations:** `+`, `-`, `/`, `*`, `%`, `^`, `(` , `)`

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and the created tag, and push the `.gem` file to [rubygems.org](https://rubygems.org).
