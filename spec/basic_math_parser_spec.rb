# frozen_string_literal: true
require 'basic_math_parser'

RSpec.describe BasicMathParser do
  it "should evaluate basic expression" do
    expect(parse("5 + 3")).to eq(8)
    expect(parse("5 - 5")).to eq(0)
    expect(parse("5 * 5")).to eq(25)
    expect(parse("6 / 4")).to eq(1.5)
    expect(parse("2^3")).to eq(8)
    expect(parse("10 % 12")).to eq(10)
  end

  it "should evaluate basic expression with parentheses" do
    expect(parse("(3 + 4) * (1 + 2)")).to eq(21)
  end

  it "should evaluate multiple operations in the expression" do
    expect(parse("3 + 4 - 2")).to eq(5)
  end

  it "should consider the order of operation" do
    expect(parse("3 + 4 * (1 + 2)")).to eq(15)
  end

  it "should raise parse error when the expression contains unsupported operations or variables" do
    expect{ parse("3+x") }.to raise_error(BasicMathParser::ParseError)
    expect{ parse("3 < 5") }.to raise_error(BasicMathParser::ParseError)
    expect{ parse("3 ++ 5") }.to raise_error(BasicMathParser::ParseError)
  end

  it "should evaluate negative numbers"

  private
  def parse(exp)
    described_class.evaluate(exp)
  end
end
